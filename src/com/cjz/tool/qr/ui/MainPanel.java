package com.cjz.tool.qr.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import com.cjz.tool.qr.QRreader;
import com.cjz.tool.qr.config.Setting;
import com.cjz.tool.qr.pojo.ParseResult;
import com.cjz.tool.qr.ui.CaptureScreenPanel.OnGetQrLinstener;
import com.cjz.tool.qr.ui.ImagePreviewPanel.OnImageDroppedListener;
import com.cjz.tool.qr.utils.Util;

import craky.componentc.JCButton;
import craky.componentc.JCTextArea;
import craky.layout.LineLayout;

public class MainPanel extends JPanel implements ActionListener, OnGetQrLinstener {

	private static final long serialVersionUID = 8763560348731595285L;

	private MainFrame mainFrame;

	private JCButton btnSelectCapture, btnCaptureFull, btnCopyResult, btnAbout;
	private JCTextArea txaResult;
	private ImagePreviewPanel imagePanel;

	public MainPanel(MainFrame jf) {

		this.mainFrame = jf;

		setPreferredSize(new Dimension(580, 300));
		setBorder(new EmptyBorder(0, 0, 0, 0));
		setOpaque(false);
		setLayout(new BorderLayout());

		EmptyComponent ecWest = new EmptyComponent();
		ecWest.setLayout(new LineLayout(15, 0, 5, 0, 0, LineLayout.CENTER, LineLayout.CENTER, LineLayout.VERTICAL));
		Border border = new EmptyBorder(5, 5, 5, 5);

		btnSelectCapture = new JCButton();
		btnSelectCapture.setText(Setting.STR_CUT_CAPTURE_AREA);
		btnSelectCapture.setBorder(border);
		btnSelectCapture.setIconTextGap(5);
		btnSelectCapture.setIcon(Setting.BUTTON_ICON_CAPTURE);
		btnSelectCapture.addActionListener(this);

		btnCaptureFull = new JCButton();
		btnCaptureFull.setText(Setting.STR_CAPTURE_FULL_AREA);
		btnCaptureFull.setBorder(border);
		btnCaptureFull.setIconTextGap(5);
		btnCaptureFull.setIcon(Setting.BUTTON_ICON_CAPTURE);
		btnCaptureFull.addActionListener(this);

		btnCopyResult = new JCButton();
		btnCopyResult.setText(Setting.STR_COPY_RESULT);
		btnCopyResult.setBorder(border);
		btnCopyResult.setIconTextGap(5);
		btnCopyResult.setIcon(Setting.BUTTON_ICON_CAPTURE);
		btnCopyResult.addActionListener(this);

		btnAbout = new JCButton();
		btnAbout.setText(Setting.STR_ABOUT);
		btnAbout.setBorder(new EmptyBorder(5, 5, 5, 5));
		btnAbout.setIconTextGap(5);
		btnAbout.setIcon(Setting.BUTTON_ICON_ABOUT);
		btnAbout.setFocusable(false);
		btnAbout.setImage(Setting.NORMAL_IMAGE);
		btnAbout.setRolloverImage(Setting.ROLLOVER_IMAGE);
		btnAbout.setPressedImage(Setting.PRESSED_IMAGE);
		btnAbout.addActionListener(this);

		EmptyComponent ecCenter = new EmptyComponent();
		ecCenter.setLayout(new GridLayout(1, 2, 5, 0));

		txaResult = new JCTextArea();
		txaResult.setAutoscrolls(true);
		txaResult.setLineWrap(true);
		txaResult.clearBorderListener();
		txaResult.setImageOnly(true);
		txaResult.setEditable(false);
		txaResult.setText(Setting.STR_TIPS_SHOW_RESULT);

		imagePanel = new ImagePreviewPanel();
		imagePanel.setOnImageDroppedListener(new OnImageDroppedListener() {

			@Override
			public void onDrop(BufferedImage image) {
				onGet(image);
			}
		});

		ecCenter.add(imagePanel);
		ecCenter.add(txaResult);

		ecWest.add(btnSelectCapture);
		ecWest.add(btnCaptureFull);
		ecWest.add(btnCopyResult);
		ecWest.add(btnAbout, LineLayout.END);

		add(ecWest, BorderLayout.WEST);
		add(ecCenter, BorderLayout.CENTER);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == btnSelectCapture) {
			mainFrame.setVisible(!Setting.hideSelfWhenCapture);
			CaptureScreenFrame csw = new CaptureScreenFrame();
			csw.setOnImageAreaSelectedListener(this);
			csw.setVisible(true);
		} else if (source == btnCaptureFull) {

			mainFrame.setVisible(!Setting.hideSelfWhenCapture);

			BufferedImage image = Util.captureScreen();
			this.onGet(image);

		} else if (source == btnCopyResult) {
			QRreader.getInstance().copy2Clipboard();
		} else if (source == btnAbout) {
			new AboutDialog();
		}
	}

	@Override
	public void onGet(BufferedImage image) {
		imagePanel.setImage(image);
		imagePanel.repaint();
		ParseResult result = Util.parseImage(image);
		QRreader.getInstance().setResult(result);
		txaResult.setText(result.getResult());
		if (Setting.autoCopyResult) {
			QRreader.getInstance().copy2Clipboard();
		}
		mainFrame.reShow();
	}

	@Override
	public void onCancel() {
		mainFrame.reShow();
	}

}
