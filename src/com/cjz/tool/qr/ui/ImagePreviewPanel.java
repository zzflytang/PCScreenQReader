package com.cjz.tool.qr.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.cjz.tool.qr.config.Setting;

public class ImagePreviewPanel extends JPanel implements DropTargetListener {

	private static final long serialVersionUID = 6250765573120218743L;

	private String tips;
	private BufferedImage image;

	public ImagePreviewPanel() {
		super();
		tips = Setting.STR_TIPS_DRAG_QR;
		new DropTarget(this, DnDConstants.ACTION_COPY_OR_MOVE, this);
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	@Override
	public void paintComponent(Graphics g1) {
		Graphics g = (Graphics) g1;
		g.setColor(Color.decode("#C1E3FA"));
		g.fillRect(0, 0, getWidth(), getHeight());
		if (null == image) {
			Font font = new Font("宋体", Font.LAYOUT_LEFT_TO_RIGHT, 12);
			g.setFont(font);
			FontMetrics fm = g.getFontMetrics();
			int tx = ((getWidth() - fm.stringWidth(tips)) / 2);
			int ty = ((getHeight() - fm.getHeight()) / 2) + fm.getAscent();
			g.setColor(Color.decode("#2E93D1"));
			g.drawString(tips, tx, ty);
			return;
		}

		g.drawImage(image, 2, 2, getWidth() - 4, getHeight() - 4, this);
		g = null;
	}

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {

	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {

	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {

	}

	@Override
	public void dragExit(DropTargetEvent dte) {

	}

	@Override
	public void drop(DropTargetDropEvent dtde) {

		if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
			dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
			try {
				Transferable tr = dtde.getTransferable();
				@SuppressWarnings("unchecked")
				List<File> list = (List<File>) tr.getTransferData(DataFlavor.javaFileListFlavor);
				File f = (File) list.get(0);
				image = ImageIO.read(f);
				if (image == null) {
					tips = Setting.STR_TIPS_PLZ_DRAG_QR;
				}
				if (onImageDroppedListener != null && image != null) {
					onImageDroppedListener.onDrop(image);
				}
			} catch (UnsupportedFlavorException ufe) {
				ufe.printStackTrace();
				image = null;
			} catch (IOException ioe) {
				ioe.printStackTrace();
				image = null;
			}
			repaint();

			// Iterator iterator = list.iterator();
			// while (iterator.hasNext()) {
			// File f = (File) iterator.next();
			// if (onFileDroppedListener != null) {
			// onFileDroppedListener.onDrop(f.getAbsolutePath());
			// }
			// }
			dtde.dropComplete(true);
			this.updateUI();
		} else {
			dtde.rejectDrop();
		}

	}

	interface OnImageDroppedListener {
		void onDrop(BufferedImage image);
	}

	OnImageDroppedListener onImageDroppedListener;

	public void setOnImageDroppedListener(OnImageDroppedListener p) {
		this.onImageDroppedListener = p;
	}

}
