package com.cjz.tool.qr.ui;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import com.cjz.tool.qr.ui.CaptureScreenPanel.OnGetQrLinstener;
import com.cjz.tool.qr.utils.Util;

public class CaptureScreenFrame extends JFrame implements WindowFocusListener {

	private static final long serialVersionUID = -6320996185254459524L;
	private CaptureScreenPanel imagePanel;

	public CaptureScreenFrame() {

		setUndecorated(true);
		setAlwaysOnTop(true);

		BufferedImage image = Util.captureScreen();
		if (image != null) {
			imagePanel = new CaptureScreenPanel(this, image);
			this.add(imagePanel);

		}

		fullScreen();

		addWindowFocusListener(this);
	}

	void fullScreen() {

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gd = ge.getDefaultScreenDevice();
		gd.setFullScreenWindow(this);
	}

	OnGetQrLinstener onGetQRListener;

	public void setOnImageAreaSelectedListener(OnGetQrLinstener l) {
		onGetQRListener = l;
		if (imagePanel != null) {
			imagePanel.setGetQRListener(l);
		}
	}

	@Override
	public void windowGainedFocus(WindowEvent e) {

	}

	@Override
	public void windowLostFocus(WindowEvent e) {
		if(onGetQRListener!=null){
			onGetQRListener.onCancel();
		}
		this.removeWindowFocusListener(this);
		this.dispose();
	}
}
